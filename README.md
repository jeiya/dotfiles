**Packages:**

- foot

- waybar

- neofetch

- neovim

- sway

- wofi

- fonts-font-awesome

- swaybg 

- swayidle 

- swayimg 

- swaylock 

**Fonts:**

- RobotoMono Nerd Font

**Acknowledgements**

- LearnLinuxTV - [sway configuration](https://www.learnlinux.tv/how-i-set-up-the-sway-window-manager-on-debian-12/)
