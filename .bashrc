# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=100000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
  export green=32
  export user=34
  export host=35
  export dir=30
  export prompt=31
    PS1='\[\e[1;${green}m\]┌─\[\e[1;${green}m\][\[\e[1;${user}m\]\u\[\e[1;${green}m\]]\[\e[1;${green}m\]\[\e[1;${green}m\]─\[\e[1;${green}m\][\[\e[1;${host}m\]\h\[\e[1;${green}m\]\[\e[1;${green}m\]] \e[1;${dir}m\]\w\n\[\e[1;${green}m\]└──\[\e[1;${green}m\][\[\e[1;${prompt}m\]\$\[\e[1;${green}m\]]\[\e[0m\] '
#    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;94m\]\u\[\033[38;5;206m\]@\[\033[1;32m\]\h\[\033[00m\] \[\033[90m\]\w\[\033[1;32m\]
#󰥭\[\033[0m\] '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Export Paths
export EDITOR=nvim

# navigation
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias doc="cd $HOME/Documents/"
alias dow="cd $HOME/Downloads/"
alias img="cd $HOME/Pictures/"
alias vid="cd $HOME/Videos/"
alias msc="cd $HOME/Music/"
alias gitre="cd $HOME/mount/storage/gitrepo/"

# typos
alias nivm="nvim"
alias trsah="trash"
alias vi="nvim"

# ls
alias dir="ls -l"
alias ll="ls -l"
alias la="ls -la"
alias l="ls -alFh"
alias ls-l="ls -l"

# general
alias grupdate="svi /etc/default/grub && sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias dirs="ls -ld */"
alias dir="ls -ld --color=auto --format=vertical */"
alias rd="sudo rm -vrfI --one-file-system"
alias srd="sudo rm -vrfI --one-file-system"
alias md="mkdir -pv"
alias smd="sudo mkdir -pv"
alias cp="cp -vrip"
alias mv="mv -vi"
alias ping="ping -c 3"
alias mp4="yt-dlp --embed-subs --no-mtime"
alias mp3="yt-dlp --embed-thumbnail --no-mtime --extract-audio --audio-format mp3 --audio-quality 0"
alias tarz="tar cvaf"
alias taru="tar xvf"
alias starz="sudo tar cvaf"
alias staru="sudo tar xvf"
alias tarzz="tar -c -I 'zstd -19 -T0' -f"
alias ssho="ssh -o PubkeyAuthentication=no"
alias sshco="ssh-copy-id"
alias bashrc=". $HOME/bashrc"
alias svi="sudoedit"
alias pinghome="sudo fping -ag 192.168.1.1 192.168.1.250 2>/dev/null"
alias flatpakclear="flatpak remove --delete-data"
alias mansearch="apropos"
alias sshkeygen="ssh-keygen -t ed25519"
alias sbstate="mokutil --sb-state"
alias waychk="echo $XDG_SESSION_TYPE"
alias rfclam="sudo freshclam"
alias scand="clamdscan --fdpass"
alias scan="clamscan"
alias h="history | grep "
alias py="python3"
alias neofetch="neofetch --ascii $HOME/.config/neofetch/debian"

# git
alias gstat="git status"
alias gbr="git branch"
alias gad="git add -A"
alias gadd="git add -A"
alias gcom="git commit -am"
alias gck="git checkout"
alias gpush="git push origin main"
alias gpushm="git push origin master"
alias gcl="git clone"
alias gsub="git submodule add"
alias gres="git restore ."

# ansible
alias ap="ansible-playbook"
alias vaulten="ansible-vault encrypt --vault-password-file $HOME/.vault_key"
alias vaultde="ansible-vault decrypt --vault-password-file $HOME/.vault_key"
alias vaulted="ansible-vault edit --vault-password-file $HOME/.vault_key"
alias vaultvi="ansible-vault edit --vault-password-file $HOME/.vault_key"
alias vaultview="ansible-vault view --vault-password-file $HOME/.vault_key"

# zypper
alias update="sudo zypper ref && sudo zypper dup && flatpak update && pipx upgrade-all && tldr -u && sudo freshclam"
alias zrm="sudo zypper rm --clean-deps"
alias zin="sudo zypper in"
alias zsr="zypper search"

# nala
alias update="sudo nala upgrade && flatpak update && pipx upgrade-all && tldr -u"
alias nrm="sudo nala autopurge && sudo nala clean"
alias nin="sudo nala install"
alias npg="sudo nala purge"
alias nsr="nala search"
alias nsh="nala show"
alias nhs="nala history"
alias nun="sudo nala history undo"

export PATH=$PATH:$HOME/.local/bin
today=$(date +'%Y_%m_%d')
